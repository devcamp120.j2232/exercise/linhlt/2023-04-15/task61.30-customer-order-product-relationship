package com.devcamp.task6070jparelationshipordercustomer.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task6070jparelationshipordercustomer.model.Order;
import com.devcamp.task6070jparelationshipordercustomer.model.Product;
import com.devcamp.task6070jparelationshipordercustomer.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;
    public List<Order> getAllOrders(){
        List<Order> orders = new ArrayList<Order>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }
    public Set<Product> getProductsByOrderId(long orderId){
        Order vOrder = orderRepository.findById(orderId);
        if ( vOrder != null){
            return vOrder.getProducts();
        }
        else return null;
    }
}
